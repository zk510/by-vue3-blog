import { defineConfig, loadEnv } from "vite";
import vue from "@vitejs/plugin-vue";
const { resolve } = require("path");
// https://vitejs.dev/config/
export default ({ mode }) => {
  const env=loadEnv(mode, process.cwd())
  const app_ip = env.VITE_API_HOST_IP;
  const isDev=mode=='development'
  const app_host = `http://${app_ip}`;
  let pageName = env.VITE_PAGE_NAME
  let pageRoot=env.VITE_PAGE_ROOT
  console.log('页面名称',pageName)
  console.log('页面人口',pageRoot)
  return defineConfig({
    plugins: [
      vue(),
    ],
    root:pageRoot,
    // 项目部署的基础路径
    build: {
      outDir: resolve(__dirname, `dist/${pageName}`),
      rollupOptions: {
        input: {
          main: resolve(__dirname, `${pageRoot}/index.html`),
        },
      },
    },
    server: {
      open: '/',
      port: 3030,
      host: "0.0.0.0",
      proxy: {
        "/apis": {
          target:'http://127.0.0.1:8000/api/v1', // 需要请求的地址
          changeOrigin: true, // 是否跨域
          rewrite: (path) => path.replace(/^\/apis/, ""),
        },
        "/music": {
          target: "https://music.liuzhijin.cn", // 需要请求的地址
          // target: import.meta.env.VUE_APP_URL,   // 需要请求的地址
          changeOrigin: true, // 是否跨域
          rewrite: (path) => path.replace(/^\/music/, ""),
        },
        "/english": {
          target: "http://sentence.iciba.com", // 需要请求的地址
          // target: import.meta.env.VUE_APP_URL,   // 需要请求的地址
          changeOrigin: true, // 是否跨域
          rewrite: (path) => path.replace(/^\/english/, ""),
        },
      },
    },
    resolve: {
      alias: [
        //配置别名
        { find: "@", replacement: resolve(__dirname, pageRoot) },
        { find: "@views", replacement: resolve(__dirname,`${pageRoot}/views`) },
      ],
      // 情景导出 package.json 配置中的exports字段
      conditions: [],
      // 导入时想要省略的扩展名列表
      // 不建议使用 .vue 影响IDE和类型支持
      extensions: ['.vue',".mjs", ".js", ".ts", ".jsx", ".tsx", ".json"],
    },
  });
};
